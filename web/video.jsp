<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Browse Alfresco Repository</title>
    <link rel="stylesheet" type="text/css" href="theme.css">
</head>
<body>
<div id="app">
    <ul id="nav" class="tabs" data-tab>
        <li class="tab-title active"><a href="/index.jsp">Get by Path</a></li>
        <li class="tab-title"><a href="/filter.jsp">Filter by file type</a></li>
        <li class="tab-title"><a href="/upload.jsp">Upload</a></li>
        <li class="tab-title"> <a href=<s:url action="loadDrop" />>Select</a></li>
        <li class="tab-title"><a href="/video.jsp">Video</a></li>
    </ul>
    <p>This page demonstrates video playback of a file hosted in Alfresco. The current version (4.0) offers a more primitive preview experience compared to the Alfresco 4.2 upgrade</p>
    <p><video width="500" height="300" controls>
        <source src="http://brainiac.byu.edu:8080/alfresco/s/api/node/content/workspace/SpacesStore/401fc05a-871f-4294-8fae-cd94b4b833cb/VaughanWilliamsTallisFantasia.mp4" type="video/mp4">
        Your browser does not support the video tag.
    </video></p>
</div>
</body>
</html>

