
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>
    <title>Upload Document</title>
    <link rel="stylesheet" type="txt/css" href="theme.css"/>
</head>
<body>
<div id="app">
    <ul class="tabs" data-tab>
        <li class="tab-title"><a href="/index.jsp">Get by Path</a></li>
        <li class="tab-title"><a href="/filter.jsp">Filter by file type</a></li>
        <li class="tab-title active"><a href="/upload.jsp">Upload</a></li>
        <li class="tab-title"> <a href=<s:url action="loadDrop" />>Select</a></li>
        <li class="tab-title"><a href="/video.jsp">Video</a></li>
    </ul>

    <form action="upload">
        <p>The default Alfresco location is the /User Homes/abbottL/ folder. This is where all uploaded files will be stored.</p>
        <br>
        <label for="file">Local path to file: <br> (ie /home/user/Documents/test.txt ) </label>
        <br>
        <input type="txt" name="objectPath"/>
        <br>
        <br>
        <label>Description: </label>
        <br>
        <textarea name="description" cols="20" rows="5">Enter file description here...
        </textarea><br>
        <br>
        <input type="submit" value="Submit"/>
    </form>


    <a href=<s:property value="docUrl"/>>
        View Uploaded File
    </a> <!--Image url is formed in the getIdAction class -->
    <p> <b>Thumbnail URL:</b> <br><s:property value="docUrlthumb"/> </p>
    <p> <b>Original File URL: </b><br><s:property value="docUrl" /> </p>

</div>

</body>
</html>

