<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Browse Alfresco Repository</title>
    <link rel="stylesheet" type="text/css" href="theme.css">
</head>
<body>
    <div id="app">
        <ul class="tabs" data-tab>
            <li class="tab-title"><a href="/index.jsp">Get by Path</a></li>
            <li class="tab-title"><a href="/filter.jsp">Filter by file type</a></li>
            <li class="tab-title"><a href="/upload.jsp">Upload</a></li>
            <li class="tab-title active"> <a href=<s:url action="loadDrop" />>Select</a></li>
            <li class="tab-title"><a href="/video.jsp">Video</a></li>
        </ul>

        <p>The query domain has been restricted to the /User Homes/abbottL/ folder. You can filter the folder contents by file extension type.
        Note that thumbnails are not automatically generated when you upload a file, so any newly uploaded files may not have a thumbnail preview.</p>

        <s:form action="submitDropdown" id="drop">
            <s:select list="filesInDirectory"
                    headerKey="-1" headerValue="Select a File to display"
                    required="true"
                    value="selectedFile"
                    name="selectedItem"
                    />
            <s:submit />
        </s:form>

        <!-- Image thumbnail displayed here. Clickable with a link to itself-->
        <a href=<s:property value="docUrl"/>>
            <img src=<s:property value="docUrlthumb"/>/>
        </a> <!--Image url is formed in the getIdAction class -->
        <p> <b>Id :</b><br> <s:property value="id"/></p>
        <p> <b>Name : </b><br> <s:property value="name"/> </p>
        <p> <b>Description : </b><br> <s:property value="description" /></p>
        <p> <b>Thumbnail URL:</b> <br><s:property value="docUrlthumb"/> </p>
        <p> <b>Original File URL: </b><br><s:property value="docUrl" /> </p>

    </div>
</body>
</html>




