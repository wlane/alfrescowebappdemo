
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Browse Alfresco Repository</title>
    <link rel="stylesheet" type="text/css" href="theme.css">
</head>

<body>
<div id="app">
<ul class="tabs" data-tab>
    <li class="tab-title"><a href="/index.jsp">Get by Path</a></li>
    <li class="tab-title active"><a href="/filter.jsp">Filter by file type</a></li>
    <li class="tab-title"><a href="/upload.jsp">Upload</a></li>
    <li class="tab-title"> <a href=<s:url action="loadDrop" />>Select</a></li>
    <li class="tab-title"><a href="/video.jsp">Video</a></li>
</ul>


<p>The query domain has been restricted to the /User Homes/abbottL/ folder. You can filter the folder contents by file extension type. </p>
<p>Note that thumbnails are not automatically generated when you upload a file, so any newly uploaded files may not have a thumbnail preview.</p>
<form action="filter">
    <input type="radio" name="filetype" value="jpg">.jpg
    <input type="radio" name="filetype" value="txt">.txt
    <input type="radio" name="filetype" value="mp3">.mp3
    <input type="radio" name="filetype" value="mp4">.mp4
    <br><br>
    <input type="submit" value="Submit"/><br>
</form>

<p> <b>Number of results:</b> <s:property value="numberOfResults"/></p>
<p> <s:iterator value="thumbUrls">
        <s:iterator>
            <img src=<s:property /><br/>
        </s:iterator>
    </s:iterator> </p>
</div>
</body>
</html>
