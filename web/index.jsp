<%--
  Created by IntelliJ IDEA.
  User: wlane
  Date: 6/13/14
  Time: 2:25 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <title>Retrieve Alfresco Document</title>
      <link rel="stylesheet" type="txt/css" href="theme.css">
  </head>
  <body>
    <div id="app">
        <ul id="nav" class="tabs" data-tab>
            <li class="tab-title active"><a href="/index.jsp">Get by Path</a></li>
            <li class="tab-title"><a href="/filter.jsp">Filter by file type</a></li>
            <li class="tab-title"><a href="/upload.jsp">Upload</a></li>
            <li class="tab-title"> <a href=<s:url action="loadDrop" />>Select</a></li>
            <li class="tab-title"><a href="/video.jsp">Video</a></li>
        </ul>

    <p>In this example you can retrieve a clickable thumbnails of cmis Documents by supplying the path relative to the alfresco directory's root folder: (ie try /User Homes/abbottL/kanaky.jpg)</p>

    <form action="retrieve">
        <label >Path:</label><br>
        <input type="txt" name="objectPath">
        <input type="submit" value="Submit"/><br>
    </form>

    <!-- Image thumbnail displayed here. Clickable with a link to itself-->
    <a href=<s:property value="docUrl"/>>
        <img src=<s:property value="docUrlthumb"/>/>
    </a> <!--Image url is formed in the getIdAction class -->
    <p> <b>Id :</b><br> <s:property value="objectId"/></p>
    <p> <b>Name : </b><br> <s:property value="docName"/> </p>
    <p> <b>Description : </b><br> <s:property value="docDesc" /></p>
    <p> <b>Thumbnail URL:</b> <br><s:property value="docUrlthumb"/> </p>
    <p> <b>Original File URL: </b><br><s:property value="docUrl" /> </p>
    <!--<p> <b>Metadata</b> s:property value="metadata"/> </p> -->
  </div>
  </body>
</html>
