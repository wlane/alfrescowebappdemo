package org.wlane.alfresco.webapp;


import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.byu.oit.cmis.CMISInterface.CMISSessionInterface;
import org.byu.oit.cmis.CMISInterface.IObjectID;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class queryAction {
    private String fileName;
    private int numberOfResults;
    private ItemIterable<QueryResult> queryResults;
    private List<String> queryResultIds;
    private List<String> thumbUrls;
    private List<String> fullUrls;

   private String filetype;


    private CMISSessionInterface session;

    public String execute(){
        System.out.println("Executing query...");

        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        session = (CMISSessionInterface)context.getBean("test");
        session.setCredentials("admin", "Iamb0b123");
        session.startSession();

        executeQuery();

        return "success";
    }

    private void executeQuery() {
        String formedQuery = formQuery(fileName);
        queryResults = session.executeQuery(formedQuery);
        queryResultIds = new ArrayList<String>();


        //count number of results, store the ides in a List
        int resultsNum=0;
        for (QueryResult item : queryResults) {
            resultsNum++;
            System.out.println("ObjectId #"+resultsNum+" " +item.getPropertyByQueryName("cmis:objectId").getFirstValue());
            queryResultIds.add((String)item.getPropertyByQueryName("cmis:objectId").getFirstValue());
        }
        numberOfResults = resultsNum;


        //form a list of thumbnails with the query results from the List of queryResultIds
        convertResultIdsToUrls(queryResultIds);
    }

    private void convertResultIdsToUrls(List<String> ids) {
        thumbUrls= new ArrayList<String>();
        fullUrls=new ArrayList<String>();

        for (int i= 0; i < ids.size(); i++){
            String[] removeVersion = ids.get(i).split(";");
            String[] extractShortId = removeVersion[0].split("/");

            thumbUrls.add(makeThumbUrl(extractShortId[extractShortId.length-1]));

        }
    }



    private String makeThumbUrl(String s) {
        StringBuilder sb = new StringBuilder();
        String ticket = session.getTicket();
        sb.append("http://brainiac.byu.edu:8080/alfresco/service/api/node/workspace/SpacesStore/" + s + "/content/thumbnails/doclib?alf_ticket=" +ticket+ "&c=queue&ph=true");


        return sb.toString();
    }

    private String formQuery(String fileName) {

        //determine folder

        IObjectID folderId =  session.getObjectIdByPath("/User Homes/abbottL/");
        Folder folder = session.getFolder(folderId.toString());


        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM cmis:document WHERE IN_TREE('"+folderId+"') AND cmis:name LIKE '");

        if(filetype.equals("jpg")){
            fileName="%.jpg%";
            sb.append(fileName);
        }
        else if(filetype.equals("txt")){
            fileName="%.txt%";
            sb.append(fileName);
        }
        else if(filetype.equals("mp3")){
            fileName="%.mp3%";
            sb.append(fileName);
        }
        else if(filetype.equals("mp4")){
            fileName="%.mp4%";
            sb.append(fileName);
        }
        sb.append("'");

        System.out.println(sb.toString());
        return sb.toString();
    }

    public void setQueryResults(ItemIterable<QueryResult> queryResults) {
        this.queryResults = queryResults;
    }

    public ItemIterable<QueryResult> getQueryResults() {
        return queryResults;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setNumberOfResults(int numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    public int getNumberOfResults() {
        return numberOfResults;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFullUrls(List<String> fullUrls) {
        this.fullUrls = fullUrls;
    }

    public void setQueryResultIds(List<String> queryResultIds) {
        this.queryResultIds = queryResultIds;
    }

    public void setThumbUrls(List<String> thumbUrls) {
        this.thumbUrls = thumbUrls;
    }

    public List<String> getFullUrls() {
        return fullUrls;
    }

    public List<String> getThumbUrls() {
        return thumbUrls;
    }

    public List<String> getQueryResultIds() {
        return queryResultIds;
    }
}
