package org.wlane.alfresco.webapp;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.byu.oit.cmis.CMISInterface.CMISSessionInterface;
import org.byu.oit.cmis.CMISInterface.IObjectID;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wlane on 6/17/14.
 */
public class dropdownRetrieveAction extends ActionSupport{
    CMISSessionInterface session;
    List<String> filesInDirectory = new ArrayList<String>();
    String selectedItem;
    String docUrlthumb;
    String docUrl;

    String id;
    String name;
    String description;

    // -----------------   action methods  -----------------------------------

    public String loadPage(){ // for when the tab is clicked and the page is initially loaded
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        session = (CMISSessionInterface)context.getBean("test");
        session.setCredentials("admin", "Iamb0b123");
        session.startSession();

        setupWorkingDirectory("/User Homes/abbottL/");

    return "success";
    }

    public String execute(){ //for when "submit" is clicked on the dropdown
        loadPage();

        IObjectID selectedId = session.getObjectIdByPath("/User Homes/abbottL/"+selectedItem);
        Document doc = session.getDocument(selectedId.toString());

        formUrls(selectedId, doc);
        extractMetadata(selectedId, doc);

        return "success";
    }

    private void extractMetadata(IObjectID selectedId, Document doc) {
        id =doc.getId();
        name =doc.getName();
        description= doc.getProperty("cm:description").getValueAsString();
    }


    private void formUrls(IObjectID docId, Document doc) {
        //Form ticket call
        StringBuilder ticket = new StringBuilder();
        ticket.append("alf_ticket=" + session.getTicket());

        //Thumbnail url
        StringBuilder sb = new StringBuilder();
        sb.append("http://brainiac.byu.edu:8080/alfresco/service/api/node/workspace/SpacesStore/" + docId.getShortID() + "/content/thumbnails/doclib?" +ticket+ "&c=queue&ph=true");
        docUrlthumb = sb.toString();

        //Full Size url
        StringBuilder fullImage = new StringBuilder();
        fullImage.append("http://brainiac.byu.edu:8080/alfresco/s/api/node/content/workspace/SpacesStore/" + docId.getShortID() + "/" + doc.getName() + "/?" +ticket);
        docUrl = fullImage.toString();
    }

    private void setupWorkingDirectory(String folderPath) {
        IObjectID objectId = session.getObjectIdByPath(folderPath);
        Folder folder = session.getFolder(objectId.toString());
        ItemIterable<CmisObject> items = session.getFolderContents(folder);

        List<String> itemNames = new ArrayList<String>();
        for(CmisObject obj : items){
            itemNames.add(obj.getName());
        }
        setFilesInDirectory(itemNames);
    }

    //getters/setters
    public void setFilesInDirectory(List<String> filesInDirectory) {
        this.filesInDirectory = filesInDirectory;
    }

    public List<String> getFilesInDirectory() {
        return filesInDirectory;
    }

    public void setSelectedItem(String selectedItem) {
        this.selectedItem = selectedItem;
    }

    public String getSelectedItem() {
        return selectedItem;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    public void setDocUrlthumb(String docUrlthumb) {
        this.docUrlthumb = docUrlthumb;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public String getDocUrlthumb() {
        return docUrlthumb;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void validate(){

        if(selectedItem!=null) {

            if (selectedItem.equals("-1")){

                addFieldError("selectedItem", "You must select one of the files to submit this form.");
                loadPage();
            }
        }
    }
}
