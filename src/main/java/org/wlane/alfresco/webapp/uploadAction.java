package org.wlane.alfresco.webapp;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Rendition;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.byu.oit.cmis.CMISAbstract.AbstractCMISSession;
import org.byu.oit.cmis.CMISInterface.CMISSessionInterface;
import org.byu.oit.cmis.CMISInterface.IObjectID;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;

public class uploadAction extends ActionSupport {

    private String objectPath;
    private String pathOnServer="/User Homes/abbottL/";
    private String description;
    private String docUrlthumb;
    private String docUrl;
    CMISSessionInterface session;

    public String execute(){

        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        session = (CMISSessionInterface)context.getBean("test");
        session.setCredentials("admin", "Iamb0b123");
        session.startSession();

        performUpload();

        return "success";
    }

    private void performUpload() {
        boolean file = trueIfFileFalseIfFolder(objectPath);
        if(file){
            // do a file upload
            String newName = extractName(objectPath);
            ContentStream cs = session.createDocument(newName, objectPath);
            IObjectID ioid = session.getObjectIdByPath(pathOnServer);

            Document doc = session.uploadDocument(ioid.getLongID(), newName, cs, "cmis:document", "1.0", description);

            IObjectID docId = session.getObjectIdByPath(pathOnServer+newName);
            formDocURl(docId,doc);
        }
        else{
            //do a folder upload -- FOLDER UPLOADS NOT SUPPORTED BY THE 'CHOOSE FILE' FORM
            String newName = extractName(objectPath);
        }

    }
    private void formDocURl(IObjectID docId, Document doc) {

        //Form ticket call
        StringBuilder ticket = new StringBuilder();
        ticket.append("alf_ticket=" + session.getTicket());

        //Thumbnail url
        StringBuilder sb = new StringBuilder();
        sb.append("http://brainiac.byu.edu:8080/alfresco/service/api/node/workspace/SpacesStore/" + docId.getShortID() + "/content/thumbnails/doclib?" +ticket+ "&c=queue&ph=true");
        docUrlthumb = sb.toString();


        //Full Size url
        StringBuilder fullImage = new StringBuilder();
        fullImage.append("http://brainiac.byu.edu:8080/alfresco/s/api/node/content/workspace/SpacesStore/" + docId.getShortID() + "/" + doc.getName() + "/?" +ticket);
        docUrl = fullImage.toString();


    }
    private String extractName(String path) {
        String[] divisions = path.split("/");

        String theName;
        if(!divisions[divisions.length-1].equals("")){
            System.out.println("the name: "+ divisions[divisions.length-1]);
            theName =  divisions[divisions.length-1];
        }
        else{
            System.out.println("the name: " + divisions[divisions.length-2]);
            theName = divisions[divisions.length-2];
        }
        return theName;
    }

    private boolean trueIfFileFalseIfFolder(String path){
        File file = new File(path);
        if (file.isDirectory())
        {
            return false;
        }
        else{
            return true;
        }
    }

    public void setObjectPath(String objectPath) {
        this.objectPath = objectPath;
    }

    public void setPathOnServer(String pathOnServer) {
        this.pathOnServer = pathOnServer;
    }

    public String getObjectPath() {
        return objectPath;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getPathOnServer() {
        return pathOnServer;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public String getDocUrlthumb() {
        return docUrlthumb;
    }

    public void setDocUrlthumb(String docUrlthumb) {
        this.docUrlthumb = docUrlthumb;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }
}
