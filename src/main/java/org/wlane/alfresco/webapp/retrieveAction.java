package org.wlane.alfresco.webapp;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Rendition;
import org.byu.oit.cmis.CMISInterface.CMISSessionInterface;
import org.byu.oit.cmis.CMISInterface.IObjectID;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class retrieveAction extends ActionSupport {
    private String objectId= "No file Path given";
    private String objectPath = "/User Homes/abbott/";
    private String docUrlthumb;
    private String docUrl;
    private String mimetype;
    private String docName;
    private String docDesc;
    private String metadata;

    CMISSessionInterface session;

    public String toObjectIdHome(){
        return "success";
    }
    public String execute(){

        System.out.println("Hello?");
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        session = (CMISSessionInterface)context.getBean("test");
        session.setCredentials("admin", "Iamb0b123");
        session.startSession();


        IObjectID folderId= session.getObjectIdByPath(objectPath);
        objectId = folderId.getLongID();

        //get thumbnail, form the URL for the thumbnail image
        Document doc = session.getDocument(folderId.getLongID());
        //Rendition nail = session.getDocumentThumbnail(doc);

        //ContentStream cs = nail.getContentStream();
        formDocURl(folderId, doc);
        extractDocData(doc);

        return "success";
    }

    private void extractDocData(Document doc) {
        docName = doc.getName();
        docDesc = doc.getProperty("cm:description").getValueAsString();
        metadata = session.getDocumentMetadataAll(doc);

    }

    private void formDocURl(IObjectID docId, Document doc) {

        //Form ticket call
        StringBuilder ticket = new StringBuilder();
        ticket.append("alf_ticket=" + session.getTicket());

        //Thumbnail url

        StringBuilder sb = new StringBuilder();
        sb.append("http://brainiac.byu.edu:8080/alfresco/service/api/node/workspace/SpacesStore/" + docId.getShortID() + "/content/thumbnails/doclib?" +ticket+ "&c=queue&ph=true");
        docUrlthumb = sb.toString();


        //Full Size url
        StringBuilder fullImage = new StringBuilder();
        fullImage.append("http://brainiac.byu.edu:8080/alfresco/s/api/node/content/workspace/SpacesStore/" + docId.getShortID() + "/" + doc.getName() + "/?" +ticket);
        docUrl = fullImage.toString();
    }

    public void setDocDesc(String docDesc) {
        this.docDesc = docDesc;
    }

    public String getDocDesc(){
        return this.docDesc;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    public String getMimetype() {
        return mimetype;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrlthumb(String docUrlthumb) {
        this.docUrlthumb = docUrlthumb;
    }

    public String getDocUrlthumb(){
        return this.docUrlthumb;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public void setObjectPath(String objectPath) {
        this.objectPath = objectPath;
    }

    public String getObjectPath(){
        return this.objectPath;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getMetadata() {
        return metadata;
    }
}
